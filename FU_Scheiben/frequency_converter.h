/*
 * frequency_converter.h
 *
 *  Created on: Apr 14, 2017
 *      Author: faebsn
 */

#ifndef FREQUENCY_CONVERTER_H_
#define FREQUENCY_CONVERTER_H_

void fc_init(void);

void fc_set_val(uint16_t);
uint16_t fc_get_val(void);
void fc_enable(void);
void fc_disable(void);


#endif /* FREQUENCY_CONVERTER_H_ */
