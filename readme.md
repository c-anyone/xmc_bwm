## Football Launching Machine Microcontroller Sourcecode

These projects represent the control software for a football launching machine and is based
on XMC4500_F100x1024 Cortex-M4 controllers from Infineon. The projects have been created
with Infineon DAVE 4.3.

### Project Structure:

		master_sled/			code of the master board and the daisy chain communication.
		magazine_slave/			code from magazine controller, needs access to master_sled/daisy_chain for compilation
		fu_scheiben/			code from frequency converters, needs access to master_sled/daisy_chain for compilation
		
### Compilation

master_sled: MASTER_DEVICE needs to be set in the preprocessor

magazine_slave: SLAVE_DEVICE_1 needs to be set in the preprocessor, folder daisy_chain has to be added in assembler and linker include path

fu_scheiben: SLAVE_DEVICE_2 needs to be set in the preprocessor, folder daisy_chain has to be added in assembler and linker include path

depending on the configuration, for the non-master boards the daisy_chain folder from master has to be added as a linked resource/folder