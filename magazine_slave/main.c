/*
 * main.c
 *
 *  Created on: 2017 Mar 30 08:48:07
 *  Author: Fabio Pungg
 */



#define SLAVE_DEVICE
#include <DAVE.h>                 //Declarations from DAVE Code Generation (includes SFR declaration)
#include <application_layer.h>
#include "elevation.h"
#include <global_definitions.h>

#define TIMER_MAG_DELAY (200000u)

/**

 * @brief main() - Application entry point
 *
 * <b>Details of function</b><br>
 * This routine is the application entry point. It is invoked by the device startup code. It is responsible for
 * invoking the APP initialization dispatcher routine - DAVE_Init() and hosting the place-holder for user application
 * code.
 */
void cb_set_param(uint8_t param, uint32_t payload);
void cb_get_param(uint8_t param);
void cb_start(uint8_t param);
void cb_request_status(void);

uint32_t timer_magazine_delay = 0;

static bool magazine_ready = false;
static void enable_mag_sensor(void) {
	NVIC_ClearPendingIRQ(MAGAZINE_SENSOR.IRQn);
	PIN_INTERRUPT_Enable(&MAGAZINE_SENSOR);
}

int main(void)
{
	DAVE_STATUS_t status;

	status = DAVE_Init();           /* Initialization of DAVE APPs  */

	if(status != DAVE_STATUS_SUCCESS)
	{
		/* Placeholder for error handler code. The while loop below can be replaced with an user error handler. */
		XMC_DEBUG("DAVE APPs initialization failed\n");
		while(1U)
		{}
	}
	XMC_USIC_CH_RXFIFO_Flush(DAISY.channel);
	application_layer_init();
	elevation_init();

	set_cmd_start_callback(cb_start);
	set_cmd_set_callback(cb_set_param);
	set_cmd_get_callback(cb_get_param);
	set_cmd_request_status_callback(cb_request_status);

	DIGITAL_IO_SetOutputHigh(&ENABLE_MAGAZINE);

	timer_magazine_delay = SYSTIMER_CreateTimer(TIMER_MAG_DELAY,SYSTIMER_MODE_ONE_SHOT,(void*)enable_mag_sensor,NULL);

	magazine_ready = true;

	while(1U)
	{
		application_worker();
		elevation_worker();
	}
}


void pwm_interrupt(void) {
	DIGITAL_IO_ToggleOutput(&PULSE_MAGAZINE);
}

void magazine_in_position(void){
	magazine_ready = true;
	TIMER_Stop(&TIMER_PWM_MAGAZINE);
	PIN_INTERRUPT_Disable(&MAGAZINE_SENSOR);
	if(elevation_ready_status()) {
		signal_ready();
	}
}

void cb_set_param(uint8_t param, uint32_t payload) {
	if(param == PARAM_ELEVATION) {
		// let's call this the elevation param
		signal_ack();
		set_elevation_to(payload);
		// move elevation where it belongs/up or down
	} // no other params are used on this board
}

void cb_start(uint8_t param) {
	signal_ack();
	if(param == 0) {
		// start something with elevation
	} else if (param == PARAM_MAGAZINE) {
		// rotate the magazine
		magazine_ready = false;
		TIMER_Start(&TIMER_PWM_MAGAZINE);
		SYSTIMER_StartTimer(timer_magazine_delay);
	} else if (param == PARAM_INIT) {
		elevation_init();
		TIMER_Stop(&TIMER_PWM_MAGAZINE);
		PIN_INTERRUPT_Disable(&MAGAZINE_SENSOR);
		// restart everything here
	}
	// no other movements are integrated here
}

void cb_request_status(void) {
	if(magazine_ready && elevation_ready_status()) {
		signal_ready();
	} else {
		signal_busy();
	}
}

void cb_get_param(uint8_t param) {
	// report the requested param value back to master
}

void elevation_ready(){
	if(magazine_ready) {
		signal_ready();
	}
}

