/*
 * frequency_converter.c
 *
 *  Created on: Apr 14, 2017
 *      Author: faebsn
 */

#include <DAVE.h>
#include "frequency_converter.h"

#define TIME_1MS		(1000u)
#define TIME_1S			(1000u*TIME_1MS)
#define RAMP_TIME		(TIME_1S)
#define RAMP_STEP_UP	(250u)
#define RAMP_STEP_DOWN	(60u)   // tested, pretty ok


static uint16_t fc_val = 0;
static uint16_t desired_val = 0;
uint32_t timer_ramp;
static uint16_t ramp_step = RAMP_STEP_UP;
static bool enabled = false;

static void set_dac(void) {
	DAC_SingleValue_SetValue_u16(&SCHEIBE_LINKS,fc_val);
	DAC_SingleValue_SetValue_u16(&SCHEIBE_RECHTS,fc_val);
}

static void ramp_cb(void) {
	if(fc_val<desired_val) {
		fc_val = (fc_val+ramp_step)>desired_val ? desired_val : fc_val+ramp_step;
	} else if(fc_val>desired_val) {
		if(fc_val>ramp_step) {
			fc_val = (fc_val-RAMP_STEP_DOWN)<desired_val ? desired_val : fc_val-RAMP_STEP_DOWN;
		} else {
			fc_val = 0;
		}
	} else if (fc_val == desired_val){
		// desired value reached, stop here, callback
		SYSTIMER_StopTimer(timer_ramp);
		if(fc_val == 0) {
			fc_disable();
		}
		// callback here
		return;
	}
	set_dac();
}

void fc_init(void) {
	timer_ramp = SYSTIMER_CreateTimer(RAMP_TIME,SYSTIMER_MODE_PERIODIC,(void*)ramp_cb,NULL);
	fc_disable();
}

void fc_set_val(uint16_t val) {
	if(val<DAC_DATA_VALUE_MAX_USIGN) {
		desired_val = val;
	} else {
		desired_val = DAC_DATA_VALUE_MAX_USIGN;
	}
	if(!enabled) {
		fc_enable();
	}
	SYSTIMER_StartTimer(timer_ramp);
}

uint16_t fc_get_val(void) {
	return desired_val; // maybe better to return fc_val
}

void fc_enable(void) {
	enabled = true;
	DIGITAL_IO_SetOutputHigh(&FLYWHEEL_ENABLE);  //Start High / Stop Low
	DIGITAL_IO_SetOutputLow(&FLYWHEEL_DIRECTION);   // Drehrichtung
}

void fc_disable(void) {
	enabled = false;
	DIGITAL_IO_SetOutputLow(&FLYWHEEL_ENABLE);
	DIGITAL_IO_SetOutputLow(&FLYWHEEL_DIRECTION);
}
