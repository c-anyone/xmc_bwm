
#include <DAVE.h>
#include <application_layer.h>
#include <global_definitions.h>
#include "frequency_converter.h"

#define FU_MAX_VALUE	(4090u)  // 10V == 50Hz
#define TIMER_READY_VAL	(10000000u)

static  uint8_t wav_trigger = 0x54;

bool ready = false;

void cb_set_param(uint8_t param, uint32_t payload);
void cb_get_param(uint8_t param);
void cb_start(uint8_t param);
void cb_request_status(void);

uint32_t ready_timer = 0;

int main (void){
	   DAVE_STATUS_t status;
	   status = DAVE_Init();


	   if(status == DAVE_STATUS_FAILURE)
	   {
	     XMC_DEBUG(("DAVE Apps initialization failed with status %d\n", status));
	     while(1U)
	     {
	     }
	   }
	   application_layer_init();
	   fc_init();
	   XMC_USIC_CH_RXFIFO_Flush(DAISY.channel);

		set_cmd_start_callback(cb_start);
		set_cmd_set_callback(cb_set_param);
		set_cmd_get_callback(cb_get_param);
		set_cmd_request_status_callback(cb_request_status);

		ready_timer = SYSTIMER_CreateTimer(TIMER_READY_VAL,SYSTIMER_MODE_ONE_SHOT,(void*)signal_ready,NULL);

	   while(1){
		   application_worker();
	   }
}

void cb_start(uint8_t param) {
	signal_ack();

	if(param == PARAM_START_FLY) {
		fc_enable();
	} else if(param == PARAM_STOP_FLY) {
		fc_disable();
	} else if(param == PARAM_PLAY_WAV) {
		UART_Transmit(&SMART_WAV,&wav_trigger,sizeof(wav_trigger));
	} else if (param == PARAM_INIT) {
		fc_set_val(0);
		// stop everything here, reinitialise

	}
	signal_ready(); // let this be triggered by a oneshot timer
}

void cb_set_param(uint8_t param, uint32_t payload) {
	signal_ack();
	if(param == PARAM_SPEED) {
		// set the dac to the value here
		fc_set_val(payload);
	}
}

void cb_request_status(void) {
	if(ready) {
		signal_ready();
	} else {
		signal_busy();
	}
}

void cb_get_param(uint8_t param) {
	if (param == 0) {
		// send the presently set fu_value or
		// it's equivalent in Hz/V to master
	}
}










