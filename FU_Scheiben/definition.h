/*
 * definition.h
 *
 *  Created on: Mar 21, 2017
 *      Author: Markus
 */

#include <DAVE.h>

#ifndef DEFINITION_H_
#define DEFINITION_H_

uint8_t dasy_chain_rec_data[255] = {0};
unsigned int dac_status;
uint16_t FU_value = 4090; // entspricht 10V
//uint16_t FU_value = 0; // entspricht 0V

#endif /* DEFINITION_H_ */
