/*
 * elevation.c
 *
 *  Created on: Apr 3, 2017
 *      Author: Fabio Pungg
 */

#include "elevation.h"

enum move_direction {
	DIR_NONE=0,
	DIR_UP,
	DIR_DOWN
};

volatile uint32_t target_value = 100;

const uint32_t limit_upper = 1100;
const uint32_t limit_lower = 100;

volatile XMC_VADC_RESULT_SIZE_t result;
volatile XMC_VADC_RESULT_SIZE_t filter = 0;

static enum move_direction dir = DIR_NONE;

static inline void stop_moving(void);
static inline void move_up(void);
static inline void move_down(void);

static inline void elevation_increase(void);
static inline void elevation_decrease(void);

void elevation_init(void) {
	ADC_MEASUREMENT_StartConversion(&ELEVATION_ADC_MEASUREMENT_0);
	dir = DIR_NONE;
	target_value = 100;
	stop_moving();
}

bool elevation_ready_status(void) {
	return (dir == DIR_NONE);
}

uint32_t elevation_get_current(void) {
	return filter;
}

void set_elevation_to(uint32_t new_target) {
	if(new_target<limit_lower) {
		target_value = limit_lower;
	} else if (new_target > limit_upper) {
		target_value = limit_upper;
	} else {
		target_value = new_target;
	}
	if(target_value>filter) {
		elevation_increase();
	} else {
		elevation_decrease();
	}
}

static inline void elevation_increase(void) {
	if(dir == DIR_DOWN) {
		stop_moving();
	}
	dir = DIR_UP;
	move_up();
}

static inline void elevation_decrease(void) {
	if(dir == DIR_UP) {
		stop_moving();
	}
	dir = DIR_DOWN;
	move_down();
}

void elevation_worker(void) {
	result = ADC_MEASUREMENT_GetResult(&ADC_MEASUREMENT_Channel_A_handle);
	filter -= (filter-result)*0.05;

	switch(dir) {
	case DIR_NONE:
		break;
	case DIR_DOWN:
		if (filter < target_value || filter < limit_lower) {
			stop_moving();
			dir = DIR_NONE;
			elevation_ready();
		}
		break;

	case DIR_UP:
		if (filter > target_value || filter > limit_upper)	{
			stop_moving();
			dir = DIR_NONE;
			elevation_ready();
		}
		break;
	}

}

static inline void stop_moving(void) {
	PWM_SetDutyCycle(&ELEVATION_PWM_0,0.0);
	PWM_SetDutyCycle(&ELEVATION_PWM_1,0.0);
}

static inline void move_up(void) {
	PWM_SetDutyCycle(&ELEVATION_PWM_0,0.0);
	PWM_SetDutyCycle(&ELEVATION_PWM_1,5000.0);
}

static inline void move_down(void) {
	PWM_SetDutyCycle(&ELEVATION_PWM_0,5000.0);
	PWM_SetDutyCycle(&ELEVATION_PWM_1,0.0);
}
