/*
 * elevation.h
 *
 *  Created on: Apr 3, 2017
 *      Author: Fabio Pungg
 */

#ifndef ELEVATION_H_
#define ELEVATION_H_

#include <DAVE.h>

void elevation_init(void);

void set_elevation_to(uint32_t);

void elevation_worker(void);
bool elevation_ready_status(void);
uint32_t elevation_get_current(void);


extern void elevation_ready(void);

#endif /* ELEVATION_H_ */
